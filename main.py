import pygame
import math
import random
from pygame import mixer
from config import *

# initialize
pygame.init()

# create screen
screen = pygame.display.set_mode((1200, 800))

# title
pygame.display.set_caption(title)


# player 1 kamra
player1Img = pygame.image.load('kamra2.png')
player1X = 600
player1Y = 750
therhold1 = 600
score1 = 0


# player 2 arnav
player2Img = pygame.image.load('arnav2.png')
player2X = 600
player2Y = 5
therhold2 = 150
score2 = 0

score = 0

#modi
modiImg = pygame.image.load('chotumodi.png')
modiX = random.randint(0,1200)
modiY = random.randint(0,800)

# planes
planeImage = pygame.image.load('plane.png')
plane1X = random.randint(0, 1200)
plane2X = random.randint(0, 1200)
plane3X = random.randint(0, 1200)
plane4X = random.randint(0, 1200)
plane5X = random.randint(0, 1200)

# holes
holeImage = pygame.image.load('space.png')
hole1X = random.randint(0, 1200)
hole2X = random.randint(0, 1200)
hole3X = random.randint(0, 1200)
hole4X = random.randint(0, 1200)
hole5X = random.randint(0, 1200)
hole6X = random.randint(0, 1200)
hole7X = random.randint(0, 1200)
hole8X = random.randint(0, 1200)
hole9X = random.randint(0, 1200)
hole10X = random.randint(0, 1200)


hole1Y = 5
hole2Y = 150
hole3Y = 300
hole4Y = 450
hole5Y = 600
hole6Y = 300
hole7Y = 450
hole8Y = 450
hole9Y = 150
hole10Y = 300


plane1Y = 80
plane2Y = 230
plane3Y = 380
plane4Y = 530
plane5Y = 680

mixer.music.load("Pim Poy Pocket.wav")
mixer.music.play(-1)

def modi(modiX, modiY):
    screen.blit(modiImg, (modiX, modiY))

def plane(planeX, planeY):
    screen.blit(planeImage, (planeX, planeY))


def hole(holeX, holeY):
    screen.blit(holeImage, (holeX, holeY))


def kamra(p1X, p1Y):
    screen.blit(player1Img, (p1X, p1Y))


def arnav(p2X, p2Y):
    screen.blit(player2Img, (p2X, p2Y))


def isCollision(enemyX, enemyY, playerX, playerY):
    distance = math.sqrt((enemyX - playerX)*(enemyX - playerX) +
                         (enemyY - playerY)*(enemyY - playerY))
    if distance < collision_sensitivity:
        return 0
    else:
        return 1


def draw_rect():
    pygame.draw.rect(screen, [7, 17, 50], [0, 0, 1200, 60])
    pygame.draw.rect(screen, [7, 17, 50], [0, 740, 1200, 60])
    pygame.draw.rect(screen, [7, 17, 50], [0, 150, 1200, 60])
    pygame.draw.rect(screen, [7, 17, 50], [0, 300, 1200, 60])
    pygame.draw.rect(screen, [7, 17, 50], [0, 450, 1200, 60])
    pygame.draw.rect(screen, [7, 17, 50], [0, 600, 1200, 60])


font = pygame.font.Font(gameOverFont, 32)


def show_score1(score_value):
    score = font.render("kamra: " + str(score_value), True, (255, 255, 255))
    screen.blit(score, (0, 30))


def show_score2(score_value):
    score = font.render("arnav : " + str(score_value), True, (255, 255, 255))
    screen.blit(score, (0, 0))


over_font = pygame.font.Font(gameOverFont, 64)


def game_over1():
    over_text = over_font.render(kamra_wins, True, (255, 255, 255))
    screen.blit(over_text, (200, 250))


def game_over2():
    over_text = over_font.render(
       arnav_wins , True, (255, 255, 255))
    screen.blit(over_text, (200, 250))


playerX_change = 0
playerY_change = 0


level = 1
chance = 0

if chance == 0:
    playerY = player1Y
    playerX = player1X
    therhold = therhold1
else:
    playerY = player2Y
    playerX = player2X
    therhold = therhold2

i = 1

# game loop
running = True
while running:

    screen.fill(background_color)
    draw_rect()

    plane_speed = planes_initial_speed + level*inc_plane_speed_by

    if score1 > MAX_SCORE:
        game_over1()
        pygame.time.delay(2000)
        running = False

    if score2 > MAX_SCORE:
        game_over2()
        pygame.time.delay(2000)
        running = False


    col = 1
    c1 = 1
    c2 = 1
    c3 = 1
    c4 = 1
    c5 = 1
    c6 = 1
    c7 = 1
    c8 = 1
    c9 = 1
    c10 = 1
    c11 = 1
    c12 = 1
    c13 = 1
    c14 = 1
    c15 = 1
    c16 = 1


    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_LEFT:
                playerX_change = -player_speed
            if event.key == pygame.K_RIGHT:
                playerX_change = player_speed
            if event.key == pygame.K_UP:
                playerY_change = -player_speed
            if event.key == pygame.K_DOWN:
                playerY_change = player_speed

        if event.type == pygame.KEYUP:
            if event.key == pygame.K_LEFT or event.key == pygame.K_RIGHT:
                playerX_change = 0
            if event.key == pygame.K_UP or event.key == pygame.K_DOWN:
                playerY_change = 0

    # collision detection
    c1 = isCollision(plane1X, plane1Y, playerX, playerY)
    c2 = isCollision(plane2X, plane2Y, playerX, playerY)
    c3 = isCollision(plane3X, plane3Y, playerX, playerY)
    c4 = isCollision(plane4X, plane4Y, playerX, playerY)
    c5 = isCollision(plane5X, plane5Y, playerX, playerY)
    c7 = isCollision(hole2X, hole2Y, playerX, playerY)
    c8 = isCollision(hole3X, hole3Y, playerX, playerY)
    c9 = isCollision(hole4X, hole4Y, playerX, playerY)
    c10 = isCollision(hole5X, hole5Y, playerX, playerY)
    c11 = isCollision(hole6X, hole6Y, playerX, playerY)
    c12 = isCollision(hole7X, hole7Y, playerX, playerY)
    c13 = isCollision(hole8X, hole8Y, playerX, playerY)
    c14 = isCollision(hole9X, hole9Y, playerX, playerY)
    c15 = isCollision(hole10X, hole10Y, playerX, playerY)
    c16 = isCollision(modiX, modiY, playerX, playerY)

    col = c1*c2*c3*c4*c5*c7*c8*c9*c10*c11*c12*c13*c14*c15*c16

    if col == 0:
        mixer_Sound = mixer.Sound('explosion.wav')
        mixer_Sound.play()
        i = 1
        pygame.time.delay(500)
        if chance == 0:
            playerX = player2X
            playerY = player2Y
            chance = 1
            therhold = therhold2
        else:
            playerX = player1X
            playerY = player1Y
            chance = 0
            therhold = therhold1

    # plane speed
    plane1X += plane_speed
    plane2X += plane_speed
    plane3X += plane_speed
    plane4X += plane_speed
    plane5X += plane_speed

    # player movement
    playerX += playerX_change
    playerY += playerY_change

    plane1X = plane1X % 1170
    plane2X = plane2X % 1170
    plane3X = plane3X % 1170
    plane4X = plane4X % 1170
    plane5X = plane5X % 1170
    plane1Y = plane1Y % 770
    plane2Y = plane2Y % 770
    plane3Y = plane3Y % 770
    plane4Y = plane4Y % 770
    plane5Y = plane5Y % 770
    # calling plane function
    plane(plane1X % 1170, plane1Y % 770)
    plane(plane2X % 1170, plane2Y % 770)
    plane(plane3X % 1170, plane3Y % 770)
    plane(plane4X % 1170, plane4Y % 770)
    plane(plane5X % 1170, plane5Y % 770)

    modi(modiX % 1170, modiY % 770)
    modiX = modiX % 1170
    modiY = modiY % 770
    modiX = modiX + modispeed * (playerX - modiX)/ math.sqrt((modiX - playerX)*(modiX - playerX) + (modiY - playerY)*(modiY - playerY))
    modiY = modiY + modispeed * (playerY - modiY)/ math.sqrt((modiX - playerX)*(modiX - playerX) + (modiY - playerY)*(modiY - playerY))

    # calling hole function
    hole(hole2X, hole2Y)
    hole(hole3X, hole3Y)
    hole(hole4X, hole4Y)
    hole(hole5X, hole5Y)
    hole(hole6X, hole6Y)
    hole(hole7X, hole7Y)
    hole(hole8X, hole8Y)
    hole(hole9X, hole9Y)
    hole(hole10X, hole10Y)

    if playerY <= 0:
        playerY = 0
    elif playerY >= 750:
        playerY = 750

    if chance == 0:
        therholds = [600, 450, 300, 150, 70, -30]
        if playerY < therhold:
            print(score1)
            therhold = therholds[i]
            i = i+1
            score1 += 1000
            if i == 6:
                level = level + 1
                i = 1
                playerX = player1X
                playerY = player1Y
                therhold = therhold1

    else:
        therholds = [150, 300, 450, 600, 740, 800]
        if playerY > therhold:
            print(score2)
            therhold = therholds[i]
            i = i+1
            score2 += 1000
            if i == 6:
                level = level + 1
                i = 1
                therhold = therhold2
                playerX = player2X
                playerY = player2Y

    show_score1(score1)
    show_score2(score2)

    if chance == 0:
        playerX = playerX % 1170
        kamra(playerX % 1170, playerY)
        if score1 > 0:
            score1 = score1 - 1
    else:
        playerX = playerX % 1170
        arnav(playerX % 1170, playerY)
        if score2 > 0:
            score2 = score2 - 1

    pygame.display.update()
